from bs4 import BeautifulSoup
import urllib.request


def getAllScripts(file_to_write_to):
    episode_dictionary = {
        "1": 6,
        "2": 22,
        "3": 23,
        "4": 14,
        "5": 26,
        "6": 24,
        "7": 24,
        "8": 24,
        "9": 23
    }
    data = ""
    for season_num, num_eps in episode_dictionary.items():
        for episode in range(1, num_eps+1):
            data += parseEpisode(season_num, episode)
    file_obj = open(file_to_write_to, "w")
    file_obj.write(data)

def parseEpisode(season_num, episode_num):
    season_num_str = str(season_num)
    episode_num_str = "0"+str(episode_num) if episode_num < 10 else str(episode_num)

    website = "http://www.officequotes.net/no"+season_num_str+"-"+episode_num_str+".php"
    print(website)
    r = urllib.request.urlopen(website).read()
    print("Episode Script " + season_num_str + "x" + str(episode_num) + " accessed")
    soup = BeautifulSoup(r, "html.parser")

    episode = ""

    for hit in soup.findAll(attrs={'class': 'quote'}):
        episode += hit.text
    return episode

if __name__ == "__main__":
    getAllScripts("data/theoffice.txt")