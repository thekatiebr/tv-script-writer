import numpy, time, sys
from keras.models import Sequential
from keras.layers import Dense
from keras.layers import Dropout
from keras.layers import LSTM
from keras.callbacks import ModelCheckpoint
from keras.utils import np_utils
from tqdm import tqdm
from GeneratePerEpoch import GeneratePerEpoch
# load ascii text and covert to lowercase
import argparse
#Code adapted from https://machinelearningmastery.com/text-generation-lstm-recurrent-neural-networks-python-keras/
def train_lstm(filename, pre_trained_weights):
    raw_text = open(filename, encoding="utf8").read()
    raw_text = raw_text.lower()
    print("loaded training data")
    # create mapping of unique chars to integers
    chars = sorted(list(set(raw_text)))
    char_to_int = dict((c, i) for i, c in enumerate(chars))
    # summarize the loaded data
    n_chars = len(raw_text)
    n_vocab = len(chars)
    print("Total Characters: ", str(n_chars))
    print("Total Vocab: " +  str(n_vocab))
    # prepare the dataset of input to output pairs encoded as integers
    seq_length = 100
    dataX = []
    dataY = []
    for i in tqdm(range(0, n_chars - seq_length, 1)):
        # sys.stdout.write(str(i)+"/"+str( n_chars - seq_length))
        # sys.stdout.flush()
        seq_in = raw_text[i:i + seq_length]
        seq_out = raw_text[i + seq_length]
        dataX.append([char_to_int[char] for char in seq_in])
        dataY.append(char_to_int[seq_out])
    n_patterns = len(dataX)
    print("Total Patterns: ", n_patterns)
    # reshape X to be [samples, time steps, features]
    X = numpy.reshape(dataX, (n_patterns, seq_length, 1))
    # normalize
    X = X / float(n_vocab)
    # one hot encode the output variable
    y = np_utils.to_categorical(dataY)
    # define the LSTM model
    model = Sequential()
    model.add(LSTM(256, input_shape=(X.shape[1], X.shape[2])))
    model.add(Dropout(0.2))
    model.add(Dense(y.shape[1], activation='softmax'))
    model.compile(loss='categorical_crossentropy', optimizer='adam')
    if pre_trained_weights is not None:
        model.load_weights(pre_trained_weights)
    # define the checkpoint
    filepath="models/weights-improvement-{epoch:02d}-{loss:.4f}.hdf5"
    checkpoint = ModelCheckpoint(filepath, monitor='loss', verbose=1, save_best_only=True, mode='min')
    #genperepoch = GeneratePerEpoch("data/theoffice_seed.txt", generate)
    callbacks_list = [checkpoint]
    # fit the model
    model.fit(X, y, epochs=10, batch_size=512, callbacks=callbacks_list)

def generate(filename, weights, output_filename="theoffice_newscript.txt"):
    raw_text = open(filename, encoding="utf8").read()
    raw_text = raw_text.lower()
    # create mapping of unique chars to integers, and a reverse mapping
    chars = sorted(list(set(raw_text)))
    char_to_int = dict((c, i) for i, c in enumerate(chars))
    int_to_char = dict((i, c) for i, c in enumerate(chars))
    # summarize the loaded data
    n_chars = len(raw_text)
    n_vocab = len(chars)
    print("Total Characters: ", n_chars)
    print("Total Vocab: ", n_vocab)
    # prepare the dataset of input to output pairs encoded as integers
    seq_length = 100
    dataX = []
    dataY = []
    for i in tqdm(range(0, n_chars - seq_length, 1)):
        seq_in = raw_text[i:i + seq_length]
        seq_out = raw_text[i + seq_length]
        dataX.append([char_to_int[char] for char in seq_in])
        dataY.append(char_to_int[seq_out])
    n_patterns = len(dataX)
    print("Total Patterns: ", n_patterns)
    # reshape X to be [samples, time steps, features]
    X = numpy.reshape(dataX, (n_patterns, seq_length, 1))
    # normalize
    X = X / float(n_vocab)
    # one hot encode the output variable
    y = np_utils.to_categorical(dataY)
    
    # define the LSTM model
    model = Sequential()
    model.add(LSTM(256, input_shape=(X.shape[1], X.shape[2])))
    model.add(Dropout(0.2))
    model.add(Dense(y.shape[1], activation='softmax'))
    # load the network weights
    model.load_weights(weights)
    model.compile(loss='categorical_crossentropy', optimizer='adam')
    #pre-written cold open
    with open("data/theoffice_seed.txt", "r") as f:
        pattern = f.read()
        f.close()
    pattern = list(pattern.lower())
    pattern = [char_to_int[value] for value in pattern]
    # pick a random seed
    # character_starts = ['M','D','J','P','O','A','P','S','K','A','T','K','R','M','C']
    #start = numpy.random.randint(0, len(dataX)-1)
    #pattern = dataX[start]
    print("Seed:")
    #print(''.join(pattern))
    print("\"", ''.join([int_to_char[value] for value in pattern]), "\"")
    file_ = open("output/theoffice_newepisode.txt", "a")
    # generate characters
    print("Generating characters")
    for i in tqdm(range(1000)):
        x = numpy.reshape(pattern, (1, len(pattern), 1))
        #print(x)
        x = x / float(n_vocab)
        #print(x)
        prediction = model.predict(x, verbose=0)
        index = numpy.argmax(prediction)
        result = int_to_char[index]
        seq_in = [int_to_char[value] for value in pattern]
        file_.write(result)
        pattern.append(index)
        pattern = pattern[1:len(pattern)]
    file_.close()
    print("\nDone.")

def main():
    parser = argparse.ArgumentParser(description='Learn to write an episode of a tv show!')
    parser.add_argument("mode", help="train = train the model, generate = generate an episode")
    parser.add_argument("file", help="path to the training data (both modes)")
    parser.add_argument("--weights", dest="weights_file")
    args = parser.parse_args()
    print(args)
    print(args)
    start = time.clock()
    if args.mode == "train":
        train_lstm(args.file, args.weight_file)
    elif args.mode == "generate":
        generate(args.file, args.weights_file)
    print("Time Elapsed: " + str(time.clock()-start))
if __name__ == "__main__":
    main()