import keras

class GeneratePerEpoch(keras.callbacks.Callback):
    def __init__(self, coldopen, generate):
        self.generate = generate
        self.coldopen = coldopen
    def on_epoch_end(self, epoch, logs={}):
        weights_file = "models/weights-improvement-0" + str(epoch)+".hdf5" if epoch < 10 else "models/weights-improvement-" + str(epoch)+".hdf5"
        self.generate(self.coldopen, weights_file, "output/theoffice_epoch"+str(epoch)+".txt")