#!/bin/bash
MODEL_SRC=lstm_v1.py
MODE=train
DATA_DIR=data
DATA_FILE=theoffice.txt
WEIGHTS_DIR=models
WEIGHTS_FILE=weights-improvement-01-2.7120.hdf5
SEED_OUTPUT=data/theoffice_seed.txt
OUTPUT_FILE=output/theoffice_newepisode.txt
cp ${SEED_OUTPUT} ${OUTPUT_FILE}

python src/${MODEL_SRC} ${MODE} ${DATA_DIR}/${DATA_FILE} --weights ${WEIGHTS_DIR}/${WEIGHTS_FILE}